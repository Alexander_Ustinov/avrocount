package com.epam.avrocount;

import org.apache.avro.mapred.AvroCollector;
import org.apache.avro.mapred.AvroReducer;
import org.apache.avro.mapred.Pair;
import org.apache.hadoop.mapred.Reporter;

import java.io.IOException;
import java.util.stream.StreamSupport;

public class AvroRowReducer extends AvroReducer<CharSequence, Long, Pair<CharSequence, Long>> {

    @Override
    public void reduce(CharSequence key, Iterable<Long> values,
                       AvroCollector<Pair<CharSequence, Long>> collector, Reporter reporter) throws IOException {
        Long count =  StreamSupport.stream(values.spliterator(), false)
                .reduce(0L, Long::sum);
        collector.collect(new Pair<>(key, count));
        System.out.println(String.format("Row count: %d", count));
    }
}
