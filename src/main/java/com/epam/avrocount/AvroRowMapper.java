package com.epam.avrocount;

import org.apache.avro.mapred.AvroCollector;
import org.apache.avro.mapred.AvroMapper;
import org.apache.avro.mapred.Pair;
import org.apache.hadoop.mapred.Reporter;

import java.io.IOException;

public class AvroRowMapper extends AvroMapper<Object, Pair<CharSequence, Long>> {

    private final String ROW_COUNT_KEY = "Row_count";
    private final Long ONE_ROW = 1L;

    @Override
    public void map(Object row, AvroCollector<Pair<CharSequence, Long>> collector, Reporter reporter)
            throws IOException {
        collector.collect(new Pair<>(ROW_COUNT_KEY, ONE_ROW));
    }
}
