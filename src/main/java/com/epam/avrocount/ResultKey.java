package com.epam.avrocount;

public enum ResultKey {
    SUCCESS(0),
    ERROR(1);
    
    private int value;

    ResultKey(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
