package com.epam.avrocount;

import org.apache.avro.Schema;
import org.apache.avro.mapred.AvroJob;
import org.apache.avro.mapred.Pair;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import org.apache.hadoop.mapred.*;

import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;

import java.io.IOException;


public class AvroCount extends Configured implements Tool {

    private static final Logger LOG = Logger.getLogger(AvroCount.class);
    private static final String JOB_NAME = "avro_count";
    private static final String SNAPPY_CODEC = "snappy";
    private static final int ARGS_COUNT = 3;


    @Override
    public int run(String[] args) throws Exception {
        if(!isValidInputArgs(args)) {
            return ResultKey.ERROR.getValue();
        }
        JobConf conf = createJobConfiguration(extractInputPath(args), extractOutputPath(args), extractSchemaPath(args));
        RunningJob job = runAndWaitForJobToFinish(conf);
        return getJobResultCode(job);
    }

    private boolean isValidInputArgs(String[] args) {
        if (args.length != ARGS_COUNT) {
            LOG.error("Bad input args");
            return false;
        }
        return true;
    }

    private Path extractInputPath(String[] args) {
        return new Path(args[0]);
    }

    private Path extractOutputPath(String[] args) {
        return new Path(args[1]);
    }

    private Path extractSchemaPath(String[] args) {
        return new Path(args[2]);
    }

    private JobConf createJobConfiguration(Path inputPath, Path outputPath, Path schemaPath) throws IOException {
        JobConf conf = createJobConfInstance();
        AvroJob.setInputSchema(conf, getSchema(schemaPath));
        AvroJob.setOutputSchema(conf,
                Pair.getPairSchema(Schema.create(Schema.Type.STRING), Schema.create(Schema.Type.LONG)));
        AvroJob.setMapperClass(conf, AvroRowMapper.class);
        AvroJob.setReducerClass(conf, AvroRowReducer.class);
        AvroJob.setOutputCodec(conf, SNAPPY_CODEC);
        FileInputFormat.setInputPaths(conf, inputPath);
        FileOutputFormat.setOutputPath(conf, outputPath);
        return conf;
    }

    private JobConf createJobConfInstance() {
        return new JobConf(AvroCount.class) {{
            setJobName(JOB_NAME);
        }};
    }

    private Schema getSchema(Path path) throws IOException {
        try (FSDataInputStream content = FileSystem.get(getConf()).open(path)) {
            return new Schema.Parser().parse(content);
        }
    }

    private RunningJob runAndWaitForJobToFinish(JobConf conf) throws IOException {
        RunningJob job = JobClient.runJob(conf);
        job.waitForCompletion();
        return job;
    }

    private int getJobResultCode(RunningJob job) throws IOException {
        if (job.isSuccessful()) {
            return ResultKey.SUCCESS.getValue();
        }
        return ResultKey.ERROR.getValue();
    }

    public static void main(String[] args) throws Exception {
        int exitCode = ToolRunner.run(new Configuration(), new AvroCount(), args);
        System.exit(exitCode);
    }
}
